package com.imdbapi.service.config;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Value;

import java.util.Collections;

import com.imdbapi.service.service.ApplicationUserService;

import static com.imdbapi.service.config.SecurityConstants.SIGN_UP_URL;
import static java.util.Arrays.asList;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

  private ApplicationUserService userService;
  
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  private String SECRET;

  public WebSecurity(ApplicationUserService userService, 
          BCryptPasswordEncoder bCryptPasswordEncoder, @Value("{jwt.SECRET}") String SECRET) {
      this.userService = userService;
      this.bCryptPasswordEncoder = bCryptPasswordEncoder;
      this.SECRET = SECRET;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
      http.cors().and().csrf().disable().authorizeRequests()
              .antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
              .antMatchers(HttpMethod.GET, "/swagger-ui.html").permitAll()
              .antMatchers(HttpMethod.GET, "/swagger-ui/**").permitAll()
              .antMatchers(HttpMethod.GET, "/swagger-resources/**").permitAll()
              .antMatchers(HttpMethod.GET, "/v3/api-docs/**").permitAll()
              .antMatchers(HttpMethod.GET, "/v2/api-docs/**").permitAll()
              .antMatchers(HttpMethod.GET, "/webjars/**").permitAll()
              .anyRequest().authenticated()
              .and()
              .addFilter(new JWTAuthenticationFilter(authenticationManager(), this.SECRET))
              .addFilter(new JWTAuthorizationFilter(authenticationManager(), this.SECRET))
              // this disables session creation on Spring Security
              .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder);
  }

  @Bean
  CorsConfigurationSource corsConfigurationSource() {
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration corsConfiguration = new CorsConfiguration();
    corsConfiguration.setAllowedOrigins(Collections.singletonList("*"));
    corsConfiguration.setAllowedMethods(asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));
    corsConfiguration.setAllowedHeaders(asList("Authorization", "Cache-Control", "Content-Type", "X-Origem"));
    corsConfiguration.setExposedHeaders(asList("Authorization", "X-Origem"));
    source.registerCorsConfiguration("/**", corsConfiguration);
    return source;
  }

}