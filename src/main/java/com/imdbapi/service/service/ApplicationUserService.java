package com.imdbapi.service.service;

import com.imdbapi.service.exception.NotFoundException;
import com.imdbapi.service.model.ApplicationUser;
import com.imdbapi.service.repository.ApplicationUserRepository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

import java.util.Optional;


@Service
public class ApplicationUserService implements UserDetailsService {

    private ApplicationUserRepository applicationUserRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public ApplicationUserService(
        ApplicationUserRepository applicationUserRepository,
        BCryptPasswordEncoder bCryptPasswordEncoder
    ) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public ApplicationUser createUser(ApplicationUser user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(true);
        return applicationUserRepository.save(user);
    }

    public void deleteUser(long id) {
        ApplicationUser user = getUser(id);
        user.setActive(false);
        applicationUserRepository.save(user);
    }

    public ApplicationUser updateUser(long id, ApplicationUser userData) {
        ApplicationUser user = getUser(id);
        user.setUsername(userData.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(userData.getPassword()));
        return applicationUserRepository.save(user);
    }

    public ApplicationUser getUser(long id) {
        Optional<ApplicationUser> applicationUser = applicationUserRepository.findById(id);
        if (!applicationUser.isPresent()) {
            throw new NotFoundException("User not found");
        }
        return applicationUser.get();
    }

    public Page<ApplicationUser> getActiveUsersPaginated(Pageable pageable) {
        return applicationUserRepository.findAllActive(pageable);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<ApplicationUser> applicationUser = applicationUserRepository.findByUsername(username);
        if (!applicationUser.isPresent()) {
            throw new UsernameNotFoundException(username);
        }
        return new User(applicationUser.get().getUsername(), applicationUser.get().getPassword(), emptyList());
    }

}