package com.imdbapi.service.service;

import com.imdbapi.service.model.Movie;

import java.util.Optional;

import com.imdbapi.service.exception.WithoutPermissionException;
import com.imdbapi.service.model.ApplicationUser;
import com.imdbapi.service.repository.ApplicationUserRepository;
import com.imdbapi.service.repository.MovieRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MovieService {

  private MovieRepository movieRepository;

  private ApplicationUserRepository userRepository;

  public MovieService(
    MovieRepository movieRepository,
    ApplicationUserRepository userRepository
  ) {
    this.movieRepository = movieRepository;
    this.userRepository= userRepository;
  }
  
  public Movie creteMovie(String username, Movie movie) {
    Optional<ApplicationUser> applicationUser = userRepository.findByUsername(username);
    if (!applicationUser.get().isAdmin()) {
      throw new WithoutPermissionException("You don't have permission");
    }
    return movieRepository.save(movie);
  }

  public Movie getMovie(long id) {
    return movieRepository.findById(id).get();
  }

  public Page<Movie> listMovies(Pageable pageable, String title, String director, String genre) {
    return movieRepository.getByFilters(
      pageable, 
      treatTerm(title), 
      treatTerm(director), 
      treatTerm(genre)
    );
  }

  private String treatTerm(String term) {
    if (term != null) return "%".concat(term).concat("%");
    else return "%".concat("").concat("%");
  }

}
