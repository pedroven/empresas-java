package com.imdbapi.service.service;

import java.util.Map;
import java.util.Optional;

import com.imdbapi.service.model.Vote;
import com.imdbapi.service.exception.BadRequestException;
import com.imdbapi.service.exception.InvalidReviewException;
import com.imdbapi.service.model.ApplicationUser;
import com.imdbapi.service.model.Movie;
import com.imdbapi.service.repository.ApplicationUserRepository;
import com.imdbapi.service.repository.MovieRepository;
import com.imdbapi.service.repository.VoteRepository;

import org.springframework.stereotype.Service;

@Service
public class VoteService {
  
  private VoteRepository voteRepository;
  private ApplicationUserRepository userRepository;
  private MovieRepository movieRepository;

  public VoteService(
    VoteRepository voteRepository,
    ApplicationUserRepository userRepository,
    MovieRepository movieRepository
  ) {
    this.voteRepository = voteRepository;
    this.userRepository = userRepository;
    this.movieRepository = movieRepository;
  }

  public Vote createVote(Map<String, Object> data, String username) {
    Optional<ApplicationUser> applicationUser = userRepository.findByUsername(username);
    long movieId = ((Number) data.get("movieId")).longValue();
    int review = ((Number) data.get("review")).intValue();
    Optional<Movie> movie = movieRepository.findById(movieId);
    validateData(review, movie);
    return voteRepository.save(
      Vote.builder()
        .movie(movie.get())
        .user(applicationUser.get())
        .review(review)
        .build()
    );
  }

  private void validateData(int review, Optional<Movie> movie) {
    if (review < 0 || review > 4) {
      throw new InvalidReviewException("Review should be a value between 0 and 4");
    }
    if (!movie.isPresent()) {
      throw new BadRequestException("Movie not found");
    }
  }

}
