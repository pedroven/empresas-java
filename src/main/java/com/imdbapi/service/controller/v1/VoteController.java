package com.imdbapi.service.controller.v1;

import java.util.Map;

import com.imdbapi.service.model.Vote;
import com.imdbapi.service.service.TokenService;
import com.imdbapi.service.service.VoteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "v1/votes")
public class VoteController {

  private VoteService voteService;

  private TokenService tokenService;

  @Autowired
  public VoteController(
    VoteService voteService,
    TokenService tokenService
  ) {
      this.voteService = voteService;
      this.tokenService = tokenService;
  }

  @PostMapping
  public Vote create(
    @RequestHeader("Authorization") String token,
    @RequestBody Map<String, Object> data
  ) {
    String username = tokenService.getUsernameFromToken(token);
    return voteService.createVote(data, username);
  }
  
}
