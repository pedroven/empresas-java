package com.imdbapi.service.controller.v1;

import java.io.UnsupportedEncodingException;

import com.imdbapi.service.model.Movie;
import com.imdbapi.service.service.MovieService;
import com.imdbapi.service.service.TokenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "v1/movies")
public class MovieController {

  private MovieService movieService;

  private TokenService tokenService;

  @Autowired
  public MovieController(
    MovieService movieService,
    TokenService tokenService
  ) {
      this.movieService = movieService;
      this.tokenService = tokenService;
  }

  @PostMapping
  public Movie create(
    @RequestHeader("Authorization") String token,
    @RequestBody Movie movie
  ) {
    String username = tokenService.getUsernameFromToken(token);
    return movieService.creteMovie(username, movie);
  }

  @GetMapping("/{id}")
  public Movie get(@PathVariable long id) {
    return movieService.getMovie(id);
  }

  @GetMapping
  public Page<Movie> list(
    Pageable pageable,
    @RequestParam(value = "title", required = false) String title,
    @RequestParam(value = "director", required = false) String director,
    @RequestParam(value = "genre", required = false) String genre
  ) throws UnsupportedEncodingException {
    System.out.println("DIRECTOR "+ director);
    System.out.println("TITLE "+ title);
    return movieService.listMovies(pageable, title, director, genre);
  }
  
}
