package com.imdbapi.service.controller.v1;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

import com.imdbapi.service.model.ApplicationUser;
import com.imdbapi.service.repository.ApplicationUserRepository;
import com.imdbapi.service.service.ApplicationUserService;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;


@RestController
@RequestMapping(value = "v1/users")
public class ApplicationUserController {

    private ApplicationUserService userService;

    @Autowired
    public ApplicationUserController(ApplicationUserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ApplicationUser signUp(@RequestBody ApplicationUser user) {
        return userService.createUser(user);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        userService.deleteUser(id);
    }
    
    @PutMapping("/{id}")
    public ApplicationUser update(@PathVariable long id, @RequestBody ApplicationUser user) {
        return userService.updateUser(id, user);
    }

    @GetMapping
    public Page<ApplicationUser> findAllActive(Pageable pageable) {
        return userService.getActiveUsersPaginated(pageable);
    }
    
}