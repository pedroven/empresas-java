package com.imdbapi.service.repository;


import com.imdbapi.service.model.Movie;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MovieRepository extends JpaRepository<Movie, Long> {

  @Query(
    nativeQuery = true,
    value = "select * from movie " + 
    "where title like :title and director like :director and genre like :genre order by review_count desc, title asc \n-- #pageable\n",
    countQuery = "select count(*) from movie " +
    "where title like :title and director like :director and genre like :genre order by review_count desc, title asc "
  )
  public Page<Movie> getByFilters(
    Pageable pageable,
    @Param("title") String title,
    @Param("director") String director,
    @Param("genre") String genre
  );

}
