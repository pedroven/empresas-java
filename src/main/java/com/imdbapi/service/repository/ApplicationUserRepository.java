package com.imdbapi.service.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

import com.imdbapi.service.model.ApplicationUser;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    Optional<ApplicationUser> findByUsername(String username); 
    
    @Query("SELECT u FROM ApplicationUser u WHERE u.active = true and u.admin = false order by u.username ASC")
    Page<ApplicationUser> findAllActive(Pageable pageable);
}
