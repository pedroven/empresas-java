CREATE TABLE movie (
	id SERIAL NOT NULL UNIQUE,
	title varchar(255) NOT NULL,
	director varchar(255) NOT NULL,
	genre varchar(255) NOT NULL,
	review_average int,
	CONSTRAINT movie_pkey PRIMARY KEY (id)
);

CREATE TABLE vote (
	id SERIAL NOT NULL UNIQUE,
	user_id int,
	movie_id int,
	review int,
	PRIMARY KEY (id),
  CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES application_user(id),
  CONSTRAINT fk_movie FOREIGN KEY (movie_id) REFERENCES movie(id)
);