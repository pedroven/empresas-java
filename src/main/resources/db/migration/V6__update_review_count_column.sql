ALTER TABLE movie ALTER review_count SET DEFAULT 0;

CREATE OR REPLACE FUNCTION update_review_count()
  RETURNS trigger AS
$$
BEGIN
    update movie set review_count = (select count(*) from vote where movie_id = movie.id)
	  where id = NEW.movie_id;
RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

create trigger count_trigger after insert on vote
FOR EACH ROW
execute procedure update_review_count()