CREATE OR REPLACE FUNCTION update_review_avg()
  RETURNS trigger AS
$$
BEGIN
    update movie set review_average = (select avg(review) from vote where movie_id = NEW.movie_id)
	where id = NEW.movie_id;
RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

create trigger avg_trigger after insert or update on vote
FOR EACH ROW
execute procedure update_review_avg()