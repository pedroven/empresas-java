CREATE TABLE application_user (
	id SERIAL NOT NULL UNIQUE,
	username varchar(255) NOT NULL UNIQUE,
	password varchar(255) NOT NULL,
	admin BOOLEAN,
	active BOOLEAN,
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

insert into application_user(username, password, admin, active) values ('admin', '$2a$10$j2xP6c0Pszm4nW9XgM7eDuXJbgGLGiI1YUjXz9biowHdkAkECXBeC', true, true);