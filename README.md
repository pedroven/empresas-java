# IMDB API

## Requisitos
 
- [__Docker__](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [__Docker compose__](https://docs.docker.com/compose/install/)

### Execução do projeto

Primeiro deve-se criar um arquivo .env com o mesmo padrão do arquivo .env.example

``` 
DATABASE_URL=jdbc:postgresql://db:5432/imdbapi
DATABASE_USERNAME=postgres
DATABASE_PASSWORD=teste
APP_SECRET=secret
```

Depois, executar o comando abaixo para gerar um arquivo jar

```sh
$ ./mvnw clean package -DskipTests
```

Executar o projeto

```sh
$ docker-compose up -d
```

Parar a execuçao
```sh
$ docker-compose down
```